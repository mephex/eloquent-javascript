for (i = 1; i <= 100; i++) {
    let string = ""
    if (i % 3 == 0) {
        string = "Fizz "
    } else if (i % 5 == 0) { 
        string = "Buzz "
    }
    if ((i % 3 == 0) && (i % 5 == 0)) {
        string = "FizzBuzz"
    } 
    console.log(string || i);
}