const min = (a, b) => {
    return(a > b ? a : b)
}

console.log(min(1,2))
console.log(min(2,1))
console.log(min(5,1))
console.log(min(4,2))