// This could be let, var, const
const fruit = "apple"

function whatKind(fruit) {
    // let will check whether a variable has been declared already and if it has, this will fail. var will overwrite the value though, const or not.
    var fruit = "Orange"
    return(fruit)
}

console.log(whatKind(fruit))