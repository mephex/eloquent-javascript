const wrapValue = (n) => {
    let local = n
    return () => local
}

const multiplier = (factor) => {
    return number => number * factor
}

let twice = multiplier(2);
console.log(twice(5))